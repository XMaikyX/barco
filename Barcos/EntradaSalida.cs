﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barcos
{
    class EntradaSalida
    {
        private DateTime entrada;

        public DateTime Entrada
        {
            get { return entrada; }
            set { entrada = value; }
        }
        private DateTime salida;

        public  DateTime Salida
        {
            get { return salida; }
            set { salida = value; }
        }
        private int posicion;

        public int Posicion
        {
            get { return posicion; }
            set { posicion = value; }
        }
        private Double CaclularDias()
        {
            TimeSpan resultado = entrada - salida;
            return resultado.TotalDays;
        }
    }
}
