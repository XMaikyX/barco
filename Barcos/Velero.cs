﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barcos
{
    class Velero : IModulo
    {
        Double proceso;
        Double precioAlquiler = 1.8;
        private Double mastiles;
        Barco barcos = new Barco();

        public Double Mastiles
        {
            get { return mastiles; }
            set { mastiles = value; }
        }
        public void PrecioLongitud()
        {
            proceso = barcos.Longitud * 0.2;
        }

        public double Resultado()
        {
            return proceso * this.Mastiles;
        }
    }
}
