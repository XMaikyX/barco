﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barcos
{
    class Deportivo : IModulo
    {
        Double proceso;
        Double precioAlquiler = 1.8;
        private double CV;
        Barco barcos = new Barco();
        public double Caballos
        {
            get { return CV; }
            set { CV = value; }
        }
        public void PrecioLongitud()
        {
            proceso = barcos.Longitud * 0.2;
        }
        public double Resultado()
        {
            return proceso * this.Caballos;
        }
    }
}
