﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barcos
{
    class Barco
    {
        private String matricula;

        public String Matricula
        {
            get { return matricula; }
            set { matricula = value; }
        }
        private Double longitud;

        public Double Longitud
        {
            get { return longitud; }
            set { longitud = value; }
        }
        private int anioDeFabricacion;

        public int AnioDeFabricacion
        {
            get { return anioDeFabricacion; }
            set { anioDeFabricacion = value; }
        }
        //modulo funcion
        private Double ModuloFuncion()
        {
            return 0.2 * this.Longitud;
        }
    }
}
